import fontforge # type: ignore

# opening YAML files
import yaml
import json

# paths
import os
# regex
import re

from typing import TypedDict

class GlyphTypedDict(TypedDict):
    glyph: int
    type: str

PATH = os.path.dirname(__file__) + '/'

CONF_DIR      = PATH + '../conf/'
ALPHABET      = open(CONF_DIR + 'alphabet.txt', 'r').read().replace('\n', '')
LIGA_PATH     = CONF_DIR + 'ligatures.yaml'

INPUT_DIR     = PATH + '../fonts/input/'
OUTPUT_DIR    = PATH + '../fonts/output/'

NULL_LOOKUP_NAME = 'NULL_LOOKUP'

def check_if_better_liga(element):
    try:
        _ = json.loads(json.dumps(element))
        return True if isinstance(element['type'], str) and isinstance(element['glyph'], int) else False
    except (KeyError, TypeError):
        return False

def resolve_regex(regex: str) -> list :
    """
    Resolve a regex using the different characters in `conf/alphabet.txt`

    As Fontforge doesn't support regex, we need to resolve the regex ourself.
    As \W (for example) would match 65000+ characters, we are using a personalized alphabet to not overwhelm the FontForge engine.
    """
    return [font[ord(char)].glyphname for char in ALPHABET if re.match(regex, char)]

def char_to_glyph(font: fontforge.font, char: str) -> fontforge.glyph :
    return font[ord(char)]

def create_null(font: fontforge.font) -> None :
    font.createChar(0x0)
    font[0x0].width = 0
    font[0x0].glyphname = 'NULL'
    font.addLookup(NULL_LOOKUP_NAME, 'gsub_single', [], [])

def change_font_name(font: fontforge.font) -> None :
    font.fontname += '-Glyphied'
    font.familyname += ' Glyphied'
    font.fullname += ' Glyphied'

def create_superscripts_n_subscripts(font: fontforge.font) -> None :
    font.addLookup('lookup.superscript', 'gsub_single', [], [])
    font.addLookupSubtable('lookup.superscript', 'lookup.superscript.sub')

    font.addLookup('lookup.subscript', 'gsub_single', [], [])
    font.addLookupSubtable('lookup.subscript', 'lookup.subscript.sub')

    for char in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789":
        superscript = font.createChar(-1, char + "_superscript_glyphizer")
        superscript.addReference(font[ord(char)].glyphname)
        superscript.width = font[ord(char)].width
        superscript.transform([0.5, 0, 0, 0.5, 0, 1000])
        font[ord(char)].addPosSub('lookup.superscript.sub', char + '_superscript_glyphizer' )
        
        subscript = font.createChar(-1, char + "_subscript_glyphizer")
        subscript.addReference(font[ord(char)].glyphname)
        subscript.width = font[ord(char)].width
        subscript.transform([0.5, 0, 0, 0.5, 0, -100])
        font[ord(char)].addPosSub('lookup.subscript.sub', char + '_subscript_glyphizer' )

def create_liga(font: fontforge.font,
                index: int,
                replacing: str, replacement: int|list[int|GlyphTypedDict],
                ahead: str|None, not_ahead: str|None,
                behind: str|None, not_behind: str|None) -> None :
    """
    Create a ligature

    @param font: The font to add the ligature in.
    @param index: The index of the current ligature.
    @param replacing: The string that will be replaced.
    @param replacement: The string that will replace the `replacing` param.
    @param ahead: A regex that will be used to match the beginning of the ligature.
    @param not_ahead: A regex that will be used to not match the beginning of the ligature.
        Used in favor of `ahead` if the number of matching characters is undefined/too big.
        For example, if we want the ligature to start with anything that is not a number, we would normally use `\D` or `[^0-9]`.
        However, Fontforge don't support regex, so that would match 65000+ characters.
        In this case, we would use `not_ahead` with `\d` or `[0-9]` which would only match 10 characters.
    @param behind: A regex that will be used to match the end of the ligature.
    @param not_behind: A regex that will be used to not match the end of the ligature.
        Used in favor of `behind` In the same context as `not_ahead` toward `ahead`.
    """

    '''
    Checking if the right conditions are met
    ''' 
    if (index < 0):
        # We check if the index is not negative
        print('index must not be negative: ' + str(index) + ' ≥ 0')
        return

    if isinstance(replacement, int):
        # We check if the replacement's glyph exist
        try:
            font[replacement]
        except:
            print('Unicode Glyph `' + hex(replacement) + '` does not exist in the font `' + font.familyname + '` for the ligature of `' + replacing + '`')
            return

    elif isinstance(replacement, list) and (
            all( isinstance(item, int) or isinstance(item['glyph'], int) ) for item in replacement
    ):
        # We check if the replacements' glyph exists
        for elem in replacement:
            try:
                if isinstance(elem, int):
                    font[elem]
                else:
                    font[elem['glyph']]
            except:
                print('Unicode Glyph `' + hex( elem if isinstance(elem, int) else elem['glyph'] ) + '` does not exist in the font `' + font.familyname + '` for the ligature of `' + replacing + '`')
                return

        # We check if the string being replaced has more (or equal) chars than the replacement
        if (len(replacing) < len(replacement)):
            print('Replacement `' + "".join([chr(glyph_index) for glyph_index in replacement]) + '` Has more characters than the replaced `' + replacing + '`')
            return

    '''
    Beginning the sorting
    '''
    ligature_name = 'glyphizer_ligature_' + str(index)
    if (ahead, not_ahead, behind, not_behind) == (None, None, None, None):
        if isinstance(replacement, int):
            create_simple_ligature(font, ligature_name, replacing, replacement)
        else:
            create_contextual_ligature(font, ligature_name, replacing, replacement)
    else:
        create_contextual_ligature(font, ligature_name, replacing, replacement, ahead, not_ahead, behind, not_behind)

def create_simple_ligature(font: fontforge.font, ligature_name: str,
                           string: str, glyph_index: int) -> None :
    """
    ### example:
    ---
    font = fontforge.open('./font.ttf')\\
    create_simple_ligature(font, 'any_string_that_is_unique', 'oe', 0x0153)
    """

    subtable_name = ligature_name # We can put anything here as long as it's unique in the Lookup
    font.addLookup(ligature_name, 'gsub_ligature', [], [['calt', [['latn', ['dflt']]]]])
    font.addLookupSubtable(ligature_name, subtable_name)
    font[glyph_index].addPosSub(subtable_name, [font[char].glyphname for char in list(string)])

def create_contextual_ligature(font: fontforge.font, ligature_name: str,
                               string: str, glyph_index: int|list[int|GlyphTypedDict],
                               ahead: str|None, not_ahead: str|None,
                               behind: str|None, not_behind: str|None) -> None :
    """
    ### examples:
    ---
    font = fontforge.open('./font.ttf')\\
    create_contextual_ligature(font, 'any_string_that_is_unique', ':fr:', [ 0x1F1EB, 0x1F1F7 ])
    ---
    font = fontforge.open('./font.ttf')\\
    create_contextual_ligature(font, 'any_string_that_is_unique', 'alpha', 0x0391, not_ahead='[a-zA-Z]', not_behind='[a-zA-Z]')
    ---
    font = fontforge.open('./font.ttf')\\
    create_contextual_ligature(font, 'any_string_that_is_unique', 'th', [ 0x1D57, 0x02B0 ], ahead='[04-9]', not_behind='[a-zA-Z0-9]')
    """

    if isinstance(glyph_index, int):
        glyph_index = [ glyph_index ]

    ahead = resolve_regex(ahead) if (ahead is not None) else None
    not_ahead = resolve_regex(not_ahead) if (not_ahead is not None) else None
    behind = resolve_regex(behind) if (behind is not None) else None
    not_behind = resolve_regex(not_behind) if (not_behind is not None) else None

    sub_ligature_name = ligature_name + '_sub'
    sub_subtable_name = string

    # for each glyph index in `glyph_index`
    # we replace the i_th char of `string` by the i_th glyph in `glyph_index`
    lookups = []
    for i in range(len(glyph_index)):
        if isinstance(glyph_index[i], int):
            lookup_name = sub_ligature_name + '_' + str(i)
            lookups.append(lookup_name)

            font.addLookup(lookup_name, 'gsub_single', [], [])
            font.addLookupSubtable(lookup_name, sub_subtable_name)
            char_to_glyph(font, string[i]).addPosSub(sub_subtable_name, font[glyph_index[i]].glyphname)
        elif check_if_better_liga(glyph_index[i]):
            if (glyph_index[i]['type'] == 'superscript'):
                lookups.append('lookup.superscript')
            elif (glyph_index[i]['type'] == 'subscript'):
                lookups.append('lookup.subscript')

    # for all the leftover glyphs that are not covered by the ligature,
    # we will show the NULL glyph
    null_subtable_name = 'NULL_' + string
    font.addLookupSubtable(NULL_LOOKUP_NAME, null_subtable_name)
    for char in string[len(glyph_index):]:
        lookups.append(NULL_LOOKUP_NAME)
        char_to_glyph(font, char).addPosSub(null_subtable_name, font[0x0].glyphname)

    font.addLookup(ligature_name, 'gsub_contextchain', [], [['calt', [['latn', ['dflt']]]]])

    # We replace all occurrences of our string with our glyph(s) starting with `ahead` and ending with `behind`
    font.addContextualSubtable(ligature_name, ligature_name, 'coverage',
                               (('[ ' + " ".join(ahead) + ' ] ') if (ahead is not None) else '') +
                               '| ' + ''.join([string[i] + ' @<' + lookups[i] + '> ' for i in range(len(string))]) + '|'
                               + ((' [ ' + " ".join(behind) + ' ]') if (behind is not None) else ''))
    # Except if it end with `not_behind`
    if (not_behind is not None):
        font.addContextualSubtable(ligature_name, ligature_name + '_not-behind', 'coverage',
                                   '| ' + (" ".join(string)) + ' | [' + (" ".join(not_behind)) + ']')
    # Except if it start with `not_ahead`
    if (not_ahead is not None):
        font.addContextualSubtable(ligature_name, ligature_name + '_not-ahead', 'coverage',
                                   '[' + (" ".join(not_ahead)) + '] | ' + (" ".join(string)) + ' |')

if __name__ == '__main__':

    with open(LIGA_PATH, 'r') as file:
        liga = yaml.safe_load(file)

    for filename in os.listdir(INPUT_DIR):
        if filename.endswith('.ttf'):
            font = fontforge.open(INPUT_DIR + filename)
            change_font_name(font)
            create_null(font)
            create_superscripts_n_subscripts(font)

            for i in range(len(liga)):

                string = liga[i]['string']
                glyph = liga[i]['glyph']
                ahead = liga[i]['ahead'] if ('ahead' in liga[i].keys()) else None
                not_ahead = liga[i]['not_ahead'] if ('not_ahead' in liga[i].keys()) else None
                behind = liga[i]['behind'] if ('behind' in liga[i].keys()) else None
                not_behind = liga[i]['not_behind'] if ('not_behind' in liga[i].keys()) else None
                
                create_liga(font, i, string, glyph, ahead=ahead, not_ahead=not_ahead, behind=behind, not_behind=not_behind)
            
            font.generate(OUTPUT_DIR + font.fontname + '.ttf')
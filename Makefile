# For Linux Only !

.SILENT:

all: build run

build:
	if [ -z "$$(docker images -q glyphizer)" ]; then \
		docker build -t glyphizer . ; \
	fi

rebuild: 
	docker build --no-cache -t glyphizer .

# docker run -it --rm -v .:/tmp/glyphizer glyphizer bash -c "python3 src/main.py 2> >(grep -Fv 'This contextual rule applies no lookups.')"
run:
	docker run -it --rm \
	-v .:/tmp/glyphizer \
	glyphizer \
	bash -c "python3 src/main.py 2> >(grep -Fv 'This contextual rule applies no lookups.')"

install: 
	mkdir -p ~/.fonts/glyphizer && \
	rm ~/.fonts/glyphizer/*.ttf && \
	cp ./fonts/output/*.ttf ~/.fonts/glyphizer && \
	fc-cache -f -v

uninstall:
	rm -rf ~/.fonts/glyphizer
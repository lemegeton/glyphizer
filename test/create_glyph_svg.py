import fontforge # type: ignore

font = fontforge.open('fira.ttf')

glyph = font.createChar(-1, 'svg_glyph_example')
glyph.width = font[0x20].width
font.createMappedChar(glyph.glyphname)
glyph.importOutlines('./test/glyph.example.svg')

font.addLookup('ligature', 'gsub_ligature', [], [['calt', [['latn', ['dflt']]]]])
font.addLookupSubtable('ligature', 'ligature.main')
glyph.addPosSub('ligature.main', ['o', 'e'])

font.generate('out.ttf')
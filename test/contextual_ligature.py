import fontforge # type: ignore

font = fontforge.open('fira.ttf')

font.createChar(0x0)
font[0x0].width = 0
font[0x0].glyphname = 'NULL'

ligatures = font.addLookup('lookup', 'gsub_single', [], [])
ligatures.addLookupSubtable('lookup', 'lookup.sub')
font['a'].addPosSub('lookup.sub', font[0x03B1].glyphname)

ligatures = font.addLookup('lookup.null', 'gsub_single', [], [])
ligatures.addLookupSubtable('lookup.null', 'lookup.sub.null')
font['l'].addPosSub('lookup.sub.null', font[0x0].glyphname)
font['p'].addPosSub('lookup.sub.null', font[0x0].glyphname)
font['h'].addPosSub('lookup.sub.null', font[0x0].glyphname)
font['a'].addPosSub('lookup.sub.null', font[0x0].glyphname)

ligatures = font.addLookup('ligature', 'gsub_contextchain', [], [['calt', [['latn', ['dflt']]]]])
ligatures.addContextualSubtable('ligature', 'ligature.main', 'coverage', 'a @<lookup> l @<lookup.null> p @<lookup.null> h @<lookup.null> a @<lookup.null>')
ligatures.addContextualSubtable('ligature', 'ligature.not-ahead', 'coverage', '[a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z] | a l p h a |')
ligatures.addContextualSubtable('ligature', 'ligature.not-behind', 'coverage', '| a l p h a | [a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z]')

font.generate('out.ttf')
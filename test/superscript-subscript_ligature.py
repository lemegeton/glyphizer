import fontforge # type: ignore

font = fontforge.open('fira.ttf')

font.addLookup('lookup.superscript', 'gsub_single', [], [])
font.addLookupSubtable('lookup.superscript', 'lookup.superscript.sub')

font.addLookup('lookup.subscript', 'gsub_single', [], [])
font.addLookupSubtable('lookup.subscript', 'lookup.subscript.sub')

for char in "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789":
    superscript = font.createChar(-1, char + "_superscript_glyphizer")
    superscript.addReference(font[ord(char)].glyphname)
    superscript.width = font[ord(char)].width
    superscript.transform([0.5, 0, 0, 0.5, 0, 1000])
    font[ord(char)].addPosSub('lookup.superscript.sub', char + '_superscript_glyphizer' )
    
    subscript = font.createChar(-1, char + "_subscript_glyphizer")
    subscript.addReference(font[ord(char)].glyphname)
    subscript.width = font[ord(char)].width
    subscript.transform([0.5, 0, 0, 0.5, 0, -100])
    font[ord(char)].addPosSub('lookup.subscript.sub', char + '_subscript_glyphizer' )

'''
Superscripts
'''
font.addLookup('superscript.st', 'gsub_contextchain', [], [['calt', [['latn', ['dflt']]]]])
font.addContextualSubtable('superscript.st', 'superscript.st.main', 'coverage', '[zero four five six seven eight nine] | t @<lookup.superscript> h @<lookup.superscript> |')
font.addContextualSubtable('superscript.st', 'superscript.st.not-behind', 'coverage', '| t h | [a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z]')

font.addLookup('superscript.nd', 'gsub_contextchain', [], [['calt', [['latn', ['dflt']]]]])
font.addContextualSubtable('superscript.nd', 'superscript.nd.main', 'coverage', '[one] | s @<lookup.superscript> t @<lookup.superscript> |')
font.addContextualSubtable('superscript.nd', 'superscript.nd.not-behind', 'coverage', '| t h | [a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z]')

font.addLookup('superscript.rd', 'gsub_contextchain', [], [['calt', [['latn', ['dflt']]]]])
font.addContextualSubtable('superscript.rd', 'superscript.rd.main', 'coverage', '[two] | n @<lookup.superscript> d @<lookup.superscript> |')
font.addContextualSubtable('superscript.rd', 'superscript.rd.not-behind', 'coverage', '| t h | [a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z]')

font.addLookup('superscript.th', 'gsub_contextchain', [], [['calt', [['latn', ['dflt']]]]])
font.addContextualSubtable('superscript.th', 'superscript.th.main', 'coverage', '[three] | r @<lookup.superscript> d @<lookup.superscript> |')
font.addContextualSubtable('superscript.th', 'superscript.th.not-behind', 'coverage', '| t h | [a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z]')

'''
Subscripts
'''
font.addLookup('subscript.H2O', 'gsub_contextchain', [], [['calt', [['latn', ['dflt']]]]])
font.addContextualSubtable('subscript.H2O', 'subscript.H2O.main', 'coverage', '| H two @<lookup.subscript> O |')
font.addContextualSubtable('subscript.H2O', 'subscript.H2O.not-ahead', 'coverage', '[a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z] | H two O |')
font.addContextualSubtable('subscript.H2O', 'subscript.H2O.not-behind', 'coverage', '| H two O | [a b c d e f g h i j k l m n o p q r s t u v w x y z A B C D E F G H I J K L M N O P Q R S T U V W X Y Z]')


font.generate('out.ttf')
import fontforge # type: ignore

font = fontforge.open('fira.ttf')

font.addLookup('ligature', 'gsub_ligature', [], [['calt', [['latn', ['dflt']]]]])
font.addLookupSubtable('ligature', 'ligature.main')
font[0x0153].addPosSub('ligature.main', ['o', 'e'])

font.generate('out.ttf')
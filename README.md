
# ![Glyphizer / Glyφzer](./.metadatas/logo.gif)

This project was inspired by the Github project [`ToxicFrog/Ligaturizer`](https://github.com/ToxicFrog/Ligaturizer).

This project allow you to create ligatures inside an already existing font.

From [Wikipedia](https://en.wikipedia.org/wiki/Ligature_(writing)) :

---

> a ligature occurs where two or more graphemes or letters are joined to form a single glyph.

> Examples are the characters `æ` and `œ` used in English and French, in which the letters `a` and `e` are joined for the first ligature and the letters `o` and `e` are joined for the second ligature.

![https://en.wikipedia.org/wiki/File:Ligature_drawing.svg](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Ligature_drawing.svg/220px-Ligature_drawing.svg.png)

---

The goal of this project is to create a tool that allow you to add custom ligatures inside an existing font.

<u>**/!\\ Ligatures are only a visual modification. It does not change the text. /!\\**</u>
(If you copy/paste the text toward a text editor without the font, everything will turn back to normal)

Can be useful for an IDE (where some compilers do not accept most of the special characters) or for a PDF file (where the font is stored into the file).

Examples of possible custom ligatures :
- replacing all occurrences of `oe` by the single glyph `œ`.
- replacing all occurrences of `th` starting by the regex `[04-9]` by `ᵗʰ`.
- replacing all occurrences of `alpha` that is neither starting nor ending with a letter by the single glyph `α`.
  - `alpha` would become `α`.
  - `alpha1` would become `α1`.
  - `alphabet` would still be `alphabet`.
- replacing all occurrences of `:D` by the single glyph `😀`.

# Story

It all started when i saw [this Stackoverflow post](https://stackoverflow.com/questions/65189559/custom-font-ligatures), which inspired me into creating a font with ligatures for each Greek letter.

My idea was simple, when i did math in python, i frequently used variables named `alpha`, `alpha1`, `alpha_1` and so on. I simply wanted to create a font that would allow me to visually transform `alpha` into `α`.

However, two problems arose :
- First of all, [`ToxicFrog/Ligaturizer`](https://github.com/ToxicFrog/Ligaturizer) is used to create ligatures by using the glyphs of the [FiraCode font](https://github.com/tonsky/FiraCode) ; which mean that the `alpha` ligature and the `α` glyph would differ.
- Secondly, [`ToxicFrog/Ligaturizer`](https://github.com/ToxicFrog/Ligaturizer) only allow the creation of "simple" ligatures, which mean if i make a ligature for `alpha`, writing `alphabet` would result into `αbet`

Henceforth, i've tried to make a simple Python script that would allow me to do what i wanted.

However, seeing the size of the project and how long it took me, i've decided to post it on [Gitlab](https://gitlab.com/lemegeton/glyphizer) in hope of helping someone with the same ~~stupid~~ ideas as me.

# Functionalities

- [x] Creation of simple ligatures.
  - Replacing a glyph by another glyph
  - Replacing a string by a single glyph
  - Replacing a string by another string
- [x] Creation of contextual ligatures.
  - Same as simple ligatures but ability to check if the string before or/and after match a regex
  - Same as simple ligatures but ability to check if the string before or/and after DO NOT match a regex
- [ ] 100% availability.
  - If a glyph does not exist for a ligature, ability to take it from another font.
    - [ ] Except if it's an accentuated glyph 
    - [x] Or a superscript/subscripts where we would dynamically create those using their original glyph as a base.
- [ ] Import own glyph
  - Ability to import SVGs to create ligatures using them.
- [ ] Ability to create "merged" ligatures.
  - Ability to create a ligature that simply replace a string by another one instead of changing just the appearance.
  - Unicode examples :
    - The unicode character 🇫 (`REGIONAL INDICATOR SYMBOL LETTER F`) and 🇷 (`REGIONAL INDICATOR SYMBOL LETTER R`) put next one to another always create the single glyph 🇫🇷 (`FRENCH FLAG REGIONAL INDICATOR SYMBOL LETTER FR`)
    - Same for accented letters where `e` and `́` are merged into the single glyph `é`
- [ ] [LaTeX](https://www.latex-project.org/).
  - Ability to use LaTeX syntaxes, or at least partially

# Personalization

## Ligatures

To add a ligature, simply go to [`./conf/ligatures.yaml`](./conf/ligatures.yaml) and follow the existing examples.

## Alphabet

To add characters to the regex engine, simply go to [`./conf/alphabet.txt`](./conf/alphabet.txt) and add your characters (By default the file contain all the ASCII characters).

# Requirements

To transform your font, all you need to have is [`Docker`]().

However, if you want to manually build your font, you will need :
- [`Python 3`](https://www.docker.com/)
- [`Python 3 - Fontforge package`](https://github.com/fontforge/fontforge) (no pip package exist as of today)
- [`Python 3 - YAML package`](https://pypi.org/project/PyYAML/)

If you wanna test your fonts before installing them, you will need to have [`Fontforge`](https://fontforge.org)

# Verify your font using Fontforge

This is not a tutorial on "How to use Fontforge".

However, it is good to know how to *verify* if your new font correspond to your need.

Once you've opened Fontforge with your font, you will see a window like this :

![Image of `Fontforge` showing the position of `Metrics > New Metrics Window`](./.metadatas/images/fontforge_metrics_window.png)

Open `Metrics > New Metrics Window` and start writing text in the text area.

![Image of the `Metrics Window` of `Fontforge`](./.metadatas/images/fontforge_metrics_window_2.png)

You should start to see text appearing just bellow with the ligatures you've defined.

# Usage / Install

This project was mainly made for Unix systems. It also work on Windows and *should* work on MacOS (thanks to the abstract layer that is Docker).

A common step to every platform is to put the fonts you want to ligaturize
in `fonts/input/`.

Once you've build your new fonts and before installing then, the best course of action is to use [`Fontforge`](https://fontforge.org) to verify them.

## On Linux

### Automatic

To automatically ligaturize your fonts, just run `make`.

Once that's done, you should be able to see all your fonts in `fonts/output/`.

Once you've verified that everything is good (using [`Fontforge`](https://fontforge.org) > Metrics > New Metrics Window), you can run `make install` to install them on your system.

If at a later date you want to uninstall them, just do `make uninstall`

### Manual

If you want to do it manually, you will first need to build the docker image with `docker build -t glyphizer .`.

Once that's done, simply run the following command :
```bash
docker run -it --rm \
-v .:/tmp/glyphizer \
glyphizer \
python3 src/main.py
```

Once you've verified that everything is good (using [`Fontforge`](https://fontforge.org) > Metrics > New Metrics Window), run the following command :
```bash
mkdir -p ~/.fonts/glyphizer && \
rm ~/.fonts/glyphizer/*.ttf && \
cp ./fonts/output/*.ttf ~/.fonts/glyphizer && \
fc-cache -f -v
```

If you want to uninstall the fonts, just do `rm -rf ~/.fonts/glyphizer`

## On other OSes

Just like the manual installation on Unix systems, with the exception of installing and uninstalling the font.

To install and uninstall, please refer to your OS's documentation.
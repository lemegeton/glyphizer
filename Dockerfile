FROM debian:bookworm
WORKDIR /tmp/glyphizer
RUN apt-get update \
 && apt-get install -y python3-fontforge python3-yaml